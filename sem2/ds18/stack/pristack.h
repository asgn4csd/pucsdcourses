#ifndef __MY_PRVSTACK_H__
#define __MY_PRVSTACK_H__

#define SIZE 1000
/* 
 * this is the private interface of our stack
 * 
 * our stack is an array of int's !!!
 *
 * */
struct stack{
 int st[SIZE];
 int ncnt;
};
#endif
