#include <malloc.h>
#include "pristack.h"
#include "stack.h"

serrorcode push (stack *s, int elt)
{
  dprint(("in push routine - \n"));
  if(s->ncnt >= SIZE)
    return serror_full;
  s->st[s->ncnt++] = elt;
  dprint(("s->ncnt = %d\ts->st[s->ncnt] = %d\n", s->ncnt, s->st[s->ncnt]));

  return ssuccess;
}
