CC = gcc

#folks who are doing DS, you must use the following flags (DSCFLAGS) to compile all your c programs in the DS course, unless otherwise stated. if your submitted programs do NOT compile with these flags, they will be rejected at once

#DSCFLAGS = -Wall -Werror -Wextra -Waggregate-return -Wcast-align -Wcast-qual -Wconversion -Wjump-misses-init -Wlogical-op -Wmissing-declarations -Wmissing-prototypes -Wnested-externs -Wpointer-arith -Wredundant-decls -Wshadow -Wstrict-prototypes -Wuninitialized -Wwrite-strings -pedantic -ansi -fno-common -fshort-enums

CFLAGS = -c #$(DSCFLAGS) #for se2 people this may do!
LCFLAGS =  #any linker flags you may wish to pass
.PHONY: all clean test

stackobjects = pop.o push.o peek.o condes.o
all: stackui

%.o: %.c
	$(CC) $(LCFLAGS) $(CFLAGS) $< -o $@

libstack.a: $(stackobjects)
	ar rcs $@ $^

stackui: libstack.a stackui.c
	$(CC) $(LCFLAGS) stackui.c libstack.a -o stackui

clean:
	rm *.o
	rm stackui

test:
	./stackui 100
	./stackui 200 
	
#the last test will fail; due to the way the stack is designed
#some advices about choosing various limits - https://glot.io/snippets/exigyk1vr2

