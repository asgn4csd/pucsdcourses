#ifndef __MY_STACK_H__
#define __MY_STACK_H__

#ifdef __MY_DEBUG_
#define dprint(x) printf x
#else
#define dprint(x) do {} while (0)
#endif

typedef enum {ssuccess=0, serror_mem, serror_mt, serror_full, serror_other} serrorcode; 
typedef struct stack stack;

/* public interface to our stack */

serrorcode construct_stack (stack **ps);
serrorcode destruct_stack (stack **ps);
serrorcode push (stack *s, int elt);
serrorcode peek (stack *s, int *ep);
serrorcode pop (stack *s);

#endif
