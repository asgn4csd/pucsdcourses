#include <stdio.h>

unsigned char checkbitn(unsigned char byte, unsigned int pos)
{
  /* write code here */
  return byte;
}

unsigned char setbitn(unsigned char byte, unsigned char flag, unsigned int pos)
{
  byte ^= (-flag ^ byte) & (1 << pos);
  return byte;
}

void printBinary(unsigned char byte)
{
  while (byte)
  {
    (byte & 1) ?  printf("1"): printf("0") ;
    byte >>= 1;
  }
  printf("\n");
}

int main(){
  unsigned char a = 0xff, bb = a, b2, b3;
  unsigned char pos = 2, flag = 0;
  b2 = setbitn(bb, flag, pos);
  b3 = setbitn(bb, flag, pos+1);
  printBinary(a);
  printBinary(b2);
  printBinary(b3);
  fprintf(stdout, "\n%d,\t %x\t\n", a, a);
  fprintf(stdout, "\n%d,\t %x\t\n", b2, b2);
  fprintf(stdout, "\n%d,\t %x\t\n", b3, b3);
  return 0;
}

