#include <malloc.h>
#include "pristack.h"
#include "stack.h"

serrorcode peek (stack *s, int *elt)
{
  if(s->ncnt <= 0)
    return serror_mt;
  *elt = s->st[s->ncnt-1];
  return ssuccess;
}
