#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "stack.h"

#define ostream stdout

/* sample use of our stack */
int main(int argc, char **argv)
{
 stack *s;
 serrorcode e;
 int elt=0,i,cnt;
 cnt = atoi(argv[1]);
 if (cnt >100)
 {
  fprintf (ostream, "don't you know our great stack doesn't support more than 100 elements, \
you loser?\n we know that 100 is enough to store all \
values in the world, \nnow get lost and come back with some more sensible value!\n \
and do NOT read that advice given at https://glot.io/snippets/exigyk1vr2\n\
believe me, 100 is the correctest limit for stack size.\n");
  return 0;
 }
 e = construct_stack (&s);
 fprintf (ostream, "address of s = %p\n", s);
 for (i=0;i<cnt;++i)
 {
  e = push(s, i);
 }

 for (i=0;i<cnt;i++)
 {
  e = peek(s,&elt);
  e = pop(s);
  fprintf (ostream, "elt = %d\n", elt);
  assert(elt==cnt-1-i); /*why??*/
 }
 fprintf (ostream, "Successful...\n");
 return 0;
}

