#include <malloc.h>
#include "pristack.h"
#include "stack.h"

serrorcode construct_stack (stack **ps)
{
  stack *s;
  if(NULL == (s = (stack *) malloc (sizeof(stack)))) 
      {
        *ps = NULL;
        return serror_mem;
      }
  *ps = s;
  (*ps)->ncnt = 0;
  return ssuccess;
}

serrorcode destruct_stack (stack **ps)
{
  if (ps!=NULL && *ps!=NULL) 
  {
    free(*ps);
    return ssuccess;
  }
  return serror_mem;
}
