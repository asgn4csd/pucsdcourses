#include <malloc.h>
#include "pristack.h"
#include "stack.h"

serrorcode pop (stack *s)
{
  dprint(("in pop routine - \n"));
  if(s->ncnt <= 0)
    return serror_mt;
  s->st[s->ncnt--] = 0;
  dprint(("s->ncnt = %d\ts->st[s->ncnt] = %d\n", s->ncnt, s->st[s->ncnt]));

  return ssuccess;
}
